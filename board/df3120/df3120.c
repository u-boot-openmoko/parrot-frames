/*
 * (C) 2006 by OpenMoko, Inc.
 * Author: Harald Welte <laforge@openmoko.org>
 *
 * based on existing S3C2410 startup code in u-boot:
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * David Mueller, ELSOFT AG, <d.mueller@elsoft.ch>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <devices.h>
#include <s3c2410.h>
#include <i2c.h>

DECLARE_GLOBAL_DATA_PTR;

//Fout=266MHz
#define M_MDIV	0x21 //m=30
#define M_PDIV	0x2  //p=4
#define M_SDIV	0x1

//USYSCLK aka Fout(upll)=96MHz
#define U_M_MDIV	0x28 //m=48
#define U_M_PDIV	0xB //p=13
#define U_M_SDIV	0x0


static inline void delay (unsigned long loops)
{
	__asm__ volatile ("1:\n"
	  "subs %0, %1, #1\n"
	  "bne 1b":"=r" (loops):"0" (loops));
}

/*
 * Miscellaneous platform dependent initialisations
 */

int board_init (void)
{
	S3C24X0_CLOCK_POWER * const clk_power = S3C24X0_GetBase_CLOCK_POWER();
	S3C24X0_GPIO * const gpio = S3C24X0_GetBase_GPIO();
	S3C24X0_LCD * const lcd = S3C24X0_GetBase_LCD();
	
	/* to reduce PLL lock time, adjust the LOCKTIME register */
	clk_power->LOCKTIME = 0xFFFFFF;

	/* configure MPLL */
	clk_power->MPLLCON = ((M_MDIV << 12) + (M_PDIV << 4) + M_SDIV);

	/* some delay between MPLL and UPLL */
	delay (4000);

	/* configure UPLL */
	clk_power->UPLLCON = ((U_M_MDIV << 12) + (U_M_PDIV << 4) + U_M_SDIV);

	/* some delay between MPLL and UPLL */
	delay (8000);

	/* set up the I/O ports */
	gpio->GPACON = 0x007FBFFF;
	gpio->GPADAT = 0x00004000; //bit 14 = /WP of nand -> we need that high

	gpio->GPBCON = 0x00110010; // backlight as output GP2
	gpio->GPBDAT = 0x00000000; // and turned OFF
	gpio->GPBUP = 0x00000000;

	gpio->GPCCON = 0xAAAA11A8;
	gpio->GPCUP = 0x00000000;

	gpio->GPDCON = 0xAAAAAAAA;
	gpio->GPDUP = 0x00000000;

	gpio->GPECON = 0xAAAAAA6A;
	gpio->GPEUP = 0x00000000;

	gpio->GPFCON = 0x000016A5;
	gpio->GPFUP = 0x0000001C;

	gpio->GPGCON = 0x552a010a;
	gpio->GPGUP = 0x00000000;

	gpio->GPHCON = 0x000aaaa;	// uart ON, no clock outs! Enable nRTS nCTS
	gpio->GPHUP = 0x00000000;
	
	/* arch number of DF3120-Board */
	gd->bd->bi_arch_number = 2589;

	/* adress of boot parameters */
	gd->bd->bi_boot_params = 0x30000100;

	icache_enable();
	dcache_enable();

	return 0;
}

u_int32_t get_board_rev(void)
{
	return 0x00000001;
}

int dram_init (void)
{
	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

/* The sum of all part_size[]s must equal to the NAND size, i.e., 0x4000000.
   "initrd" is sized such that it can hold two uncompressed 16 bit 640*480
   images: 640*480*2*2 = 1228800 < 1245184. */

unsigned int dynpart_size[] = {
    CFG_UBOOT_SIZE, 0x4000, 0x200000, 0xa0000, 0x3d5c000-CFG_UBOOT_SIZE, 0 };
char *dynpart_names[] = {
    "u-boot", "u-boot_env", "kernel", "splash", "rootfs", NULL };


